import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AppointmentHistory from './AppointmentHistory';
import Sales_PersonForm from './Sales_PersonForm';
import Potential_CustomerForm from './Potential_CustomerForm';
import Sale_RecordForm from './Sale_RecordForm';
import ManufacturerList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import { useEffect, useState } from 'react';
import VehicleModelsList from './VehicleModelsList';
import VehicleModelsForm  from './VehicleModelsForm';
import SalesList from './SalesList';
import EmployeeSalesList from './EmployeeSalesList';


function App(props) {
  let [automobile, setAutomobile] =useState(
    {
      name: '',
      color: '', 
      year: '', 
      vin: '', 
      model: ''
})
  
  async function fetchAutomobile(){
    const res = await fetch('http://localhost:8100/api/automobiles');
    const newAutomobile = await res.json();
    setAutomobile(newAutomobile)
  }

  let [model, setModel] = useState({
    name: '',
    picture_url: '',
    manufacturer: '',
  })
  async function fetchModel() {
    const res = await fetch('http://localhost:8100/api/models');
    const NewModel = await res.json();
    setModel(NewModel)
  }

  let [manufacturer, setManufacturer] = useState({name: []})
  async function fetchManufacturer() {
    const res = await fetch('http://localhost:8100/api/manufacturers');
    const NewManufacturer = await res.json();
    
    setManufacturer(NewManufacturer.manufacturers)
    
  }

  useEffect(() => {
    fetchAutomobile()
    fetchModel()
    fetchManufacturer()
    
  }, [])



  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        
          <Route path="/" element={<MainPage />} />
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm technicians={props.technicians} />} />
          </Route>
          <Route path="appointments">
            <Route path="/appointments" element={<AppointmentList appointments={props.appointments} />} />
          </Route>
          <Route path="appointments">
            <Route path="history" element={<AppointmentHistory appointments={props.appointments} />} />
          </Route>
          <Route path="appointments">
            <Route path="new" element={<AppointmentForm appointments={props.appointments} />} />
          </Route>
          <Route path="automobiles">
            <Route path="/automobiles/" element={<AutomobileList automobiles={automobile}/>} />
          </Route>
          <Route path="automobiles">
            <Route path="create" element={ <AutomobileForm />} />
          </Route>
          <Route path="sales">
            <Route path="create" element={ <Sales_PersonForm sales={props.sales_person}/>} />
          </Route>
          <Route path="sales/potential_customer">
            <Route path="create" element={ <Potential_CustomerForm sales={props.potential_customer}/>} />
          </Route>
          <Route path="sales/sale_record">
            <Route path="create" element={ <Sale_RecordForm sales={props.sale_record}/>} />
          </Route>
          <Route path="sales">
            <Route path="sales_list" element={ <SalesList sales={props.sales}/>} />
          </Route>
          <Route path="sales">
            <Route path="employee_sales_list" element={ <EmployeeSalesList employee_sales={props.sales}/>} />
          </Route>
          <Route path="manufacturers">
            <Route path="/manufacturers/" element={ <ManufacturerList manufacturers={manufacturer}/>} />
            </Route>
            <Route path="manufacturers">
            <Route path="create" element={ <ManufacturerForm manufacturers={manufacturer}/>} />
          </Route>
          <Route path="models">
            <Route path="/models/" element={ <VehicleModelsList models={model}/>} />
          </Route>
          <Route path="models">
           
            <Route path="create" element={ <VehicleModelsForm/>} />
          </Route>
          
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
